import React from 'react'

export default class ItemFakeComment extends React.Component{
  render(){
    return(
      <>
        <div className="flex flex-col h-full justify-between p-4 mb-2 animate-pulse" aria-label="Chargement des données en cours...">
          <div className="post-content">
            <span className="block h-2 w-1/2 mb-4 bg-gray-200 rounded-lg" aria-hidden="true">&nbsp;</span>
            <span className="block h-3 w-3/4 mb-4 bg-indigo-200 rounded-full" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-full mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-full mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-1/3 mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
          </div>
        </div>
      </>
    )
  }
}
