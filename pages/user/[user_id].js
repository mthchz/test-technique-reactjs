import { useRouter } from 'next/router'

import Head from "next/head";
import Header from '../../components/Header'
import ListPosts from '../../components/ListPosts';

export default function UserPosts() {
  const router = useRouter();
  const { user_id, name } = router.query;

  return (
    <>
      <Head>
        <title>{name ? name + ' : ' : ''}Liste des publications</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="wrapper">
        <Header />

        <main className="container mx-auto p-6">
          <ListPosts userId={user_id} />
        </main>
      </div>
    </>
  )
}
