import React from 'react'

import ItemFakeComment from "./ItemFakeComment";
import ItemComment from "./ItemComment";

const initialState = {
  isOpen: false,
  isLoading: false,
  isFullyLoaded: false,
  postId: 0,
  listComments: []
};

export default class ModalComments extends React.Component{
  constructor(props) {
    super(props);

    this.state = initialState;

    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.state.isFullyLoaded){
      return;
    }
    if(this.props.openModal && !this.state.isLoading && !this.state.isOpen){ // On open
      this.setState({
        isLoading: true,
        isOpen: true
      });
      this.getComments();
    }
  }

  getComments() {
    setTimeout(() => { // Création d'un décalage à l'appel pour ne pas faire scintiller l'effet de chargement
      fetch(`https://jsonplaceholder.typicode.com/comments?postId=${this.props.postId}`)
        .then( res => res.json())
        .then( json => {
          this.setState({
            isLoading: false,
            isFullyLoaded: true,
            listComments: json
          });
        });
    }, 500);
  }

  handleCloseModal() {
    this.props.onCloseModal();
    this.setState(initialState);
  }

  render(){
    const renderCommentsList = () => {
      if (this.state.isLoading) {
        return (
          <>
            <ItemFakeComment />
            <ItemFakeComment />
            <ItemFakeComment />
          </>
        );
      } else if (this.state.isFullyLoaded && this.state.listComments.length > 0) {
        return this.state.listComments.map((comment, i) => (
          <ItemComment comment={comment} key={comment.email.toString()+'-'+i.toString()} />
        ))
      } else {
        return (
          <p className="mt-2 mb-4 text-gray-700 italic">Pas de commentaires pour cette publication</p>
        );
      }
    };

    return(
      <>
        {this.state.isOpen && <div className="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
          <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
            <div className="fixed inset-0 bg-gray-800 bg-opacity-75 transition-opacity" aria-hidden="true">&nbsp;</div>
            <span className="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

            <div className="inline-block w-full sm:max-w-lg  align-bottom sm:my-8 sm:align-middle text-left bg-white overflow-hidden rounded-lg shadow-xl transform transition-all">
              <div className="px-4 pt-5 bg-white">
                <div className="flex items-start justify-between w-full">
                  <h3 className="mb-4 text-lg leading-6 font-medium text-gray-900" id="modal-title">
                    Commentaires ({this.state.listComments.length})
                  </h3>
                  <button className="btn-none" onClick={this.handleCloseModal}>&#x2715; Quitter</button>
                </div>

                <div>
                  {renderCommentsList()}
                </div>
              </div>
              <div className="p-2 text-gray-400 text-sm text-right italic bg-gray-100">
                <button onClick={this.handleCloseModal}>Quitter</button>
              </div>
              </div>
          </div>
        </div>}
      </>
    )
  }
}
