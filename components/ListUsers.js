import React from 'react';
import ItemUser from "./ItemUser";
import ItemFakeUser from "./ItemFakeUser";

export default class ListUsers extends React.Component{
  constructor(props) {
    super(props);

    this.users = [];

    this.state = {
      isLoading: true,
      listUsers: []
    };
  };

  componentDidMount(){
    setTimeout(() => { // Création d'un décalage à l'appel pour ne pas faire scintiller l'effet de chargement
      fetch('https://jsonplaceholder.typicode.com/users')
        .then( res => res.json())
        .then( json => {
          this.users = json;

          this.setState({
            isLoading: false,
            listUsers: json
          })
        });
    }, 500);
  }

  searchUsers = (event) => {
    const keyword = event.target.value.toLowerCase();
    let result = this.users.filter(user => {
      return user.name.toLowerCase().includes( keyword ) || user.email.toLowerCase().includes( keyword )
    });

    this.setState({ listUsers: result})
  };

  onSubmit = (event) => event.preventDefault();

  render (){
    const renderUsersList = () => {
      if (this.state.isLoading) {
        return (
          <>
            <ItemFakeUser />
            <ItemFakeUser />
            <ItemFakeUser />
          </>
        );
      } else if (!this.state.isLoading && this.state.listUsers.length > 0) {
        return this.state.listUsers.map((user, i) => (
          <ItemUser key={user.id.toString()+'-'+i.toString()} user={user} />
        ))
      } else {
        return <div className="w-full p-4 my-4 text-center text-gray-400 italic">La recherche ne trouve rien...</div>
      }
    };

    return (
        <div className="">
          <form className="form-search mb-10" onSubmit={this.onSubmit}>
            <label htmlFor="search" className="sr-only">Rechercher un utilisateur</label>
            <input disabled={this.state.isLoading} id="search" name="search" className="field-search mb-6" onChange={this.searchUsers} type="text" placeholder="Rechercher par nom ou email..." autoComplete="off"/>
          </form>

          <div className="bg-white border border-gray-200 rounded-lg shadow">
            <div className="sticky top-0 table-line text-xs font-medium uppercase text-indigo-600 bg-gray-100 rounded-t-lg">
              <span>Nom</span>
              <span>Pseudo</span>
              <span>Email</span>
              <span></span>
            </div>

            {renderUsersList()}
          </div>
        </div>
      )
  }
}
