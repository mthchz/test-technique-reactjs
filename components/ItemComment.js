import React from 'react'

export default class ItemComment extends React.Component{
  render(){
    return(
      <>
        <div className="py-4 border-t border-gray-200">
          <span className="mb-2 italic text-right text-gray-700">Par {this.props.comment.email}</span>
          <h4 className="text-lg font-medium text-indigo-700">{this.props.comment.name}</h4>
          <p>{this.props.comment.body}</p>
        </div>
      </>
    )
  }
}
