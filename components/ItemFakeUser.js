import React from 'react'
import Link from "next/link";

export default class ItemUser extends React.Component{
  render(){
    return(
      <>
        <div className="table-line animate-pulse" aria-label="Chargement des données en cours...">
          <span className="block h-3 w-1/2 mb-2 lg:mb-0 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
          <span className="block h-3 w-1/3 mb-2 lg:mb-0 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
          <span className="block h-3 w-1/3 mb-2 lg:mb-0 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
          <span className="block place-self-end h-3 w-2/3 bg-indigo-400 rounded-lg" aria-hidden="true">&nbsp;</span>
        </div>
      </>
    )
  }
}
