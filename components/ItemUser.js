import React from 'react'
import Link from "next/link";

export default class ItemUser extends React.Component{
  render(){
    return(
      <>
        <div className="table-line">
          <span className="truncate">
            {this.props.user.name}
          </span>

          <span className="truncate">
            {this.props.user.username}
          </span>

          <span className="truncate">
            {this.props.user.email}
          </span>

          <Link href={{ pathname: `/user/${this.props.user.id}`, query: { name: this.props.user.name.toString() } }}>
            <a className="font-bold text-right text-indigo-600 truncate hover:text-indigo-700">
              Voir les publications
            </a>
          </Link>
        </div>
      </>
    )
  }
}
