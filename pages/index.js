import Head from 'next/head'

import Header from '../components/Header'
import ListUsers from "../components/ListUsers"

export default function Home() {
  return (
    <>
      <Head>
        <title>Liste des utilisateurs</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <div className="wrapper">
        <Header />

        <main className="container mx-auto p-6">
          <ListUsers />
        </main>
      </div>
    </>
  )
}
