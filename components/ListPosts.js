import React from 'react';

import ItemPost from "./ItemPost";
import ItemFakePost from "./ItemFakePost";
import ModalComments from "./ModalComments";

export default class ListPosts extends React.Component{
  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);

    this.state = {
      user: {},
      isLoading: true,
      listPosts: [],
      formState: {
        isSubmittedSuccessful: false,
        isSubmitting: false
      },
      formValue: {
        title: '',
        body: ''
      },
      formError: '',
      openModalComments: false,
      modalCommentsPostId: ''
    }
  }

  componentDidMount(){
    setTimeout(() => { // Création d'un décalage à l'appel pour ne pas faire scintiller l'effet de chargement
      // Get user detail
      fetch(`https://jsonplaceholder.typicode.com/users/${this.props.userId}`)
        .then( res => res.json())
        .then( json => {
          this.setState({
            user: json
          });
        });

      // Get posts list
      fetch(`https://jsonplaceholder.typicode.com/posts?userId=${this.props.userId}`)
        .then( res => res.json())
        .then( json => {
          this.setState({
            isLoading: false,
            listPosts: json
          })
        });
    }, 500);
  }

  handleChange(e) {
    // Init after submit
    if(this.state.formState.isSubmittedSuccessful){
      this.setState( {
        formState:{
          isSubmittedSuccessful: false,
          isSubmitting: false
        }
      });
    }

    // Update state value
    if(e.target.id === 'title' || 'body'){
      this.setState( (state) => ({
        formValue: {
          ...state.formValue,
          [e.target.id]: e.target.value.toString(),
        }
      }));
    }
  }

  handleSubmit(e) {
    e.preventDefault();

    // Errors
    if(!this.state.formValue.title){
      this.setState({formError: 'Le titre ne peut être vide'});
      return false;
    } else if(!this.state.formValue.body){
      this.setState({formError: 'Le contenu ne peut être vide'});
      return false;
    } else {
      this.setState({formError: ''});
    }

    // Set submitting
    this.setState( (state) => ({
      formState: {
        ...state.formState,
        isSubmitting: true
      }
    }));

    // Send fields values
    fetch('https://jsonplaceholder.typicode.com/posts', {
      method: 'POST',
      body: JSON.stringify({
        userId: this.props.userId.toString(),
        title: this.state.formValue.title.toString(),
        body: this.state.formValue.body.toString(),
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    })
      .then(
        (res) => res.json(),
        (error) => {
          console.error(error);

          this.setState({
            formError: "Erreur d'envoi, réessayez ultérieurement...",
            formState: {
              isSubmitting: false,
              isSubmittedSuccessful: false
            }
          });
        }
      )
      .then((json) => {
        this.setState((state) => ({
          listPosts: [json, ...state.listPosts],
          formState: {
            isSubmitting: false,
            isSubmittedSuccessful: true
          },
          formValue: {
            title: '',
            body: ''
          }
        }));
      });
  }

  handleOpenModal(postId){
    this.setState({
      openModalComments: true,
      modalCommentsPostId: postId.toString()
    });
  }

  handleCloseModal(){
    this.setState({openModalComments: false});
  }

  render (){
    const renderTitle = () => {
      if(Object.keys(this.state.user).length > 0){
        return (
          <h1 className="inline-block w-full mb-8 pb-2 text-3xl border-b-4 border-indigo-600">
            {this.state.user.name}<span className="ml-2 text-lg text-gray-700">alias {this.state.user.username}</span>
          </h1>
        );
      } else {
        return(
          <h1 className="inline-block w-full mb-8 pb-2 text-3xl border-b-4 border-indigo-600">
            <span className="block h-6 w-1/2 mb-2 bg-gray-400 rounded-full animate-pulse" aria-hidden="true">&nbsp;</span>
          </h1>
        );
      }
    };

    const renderPostsList = () => {
      if (this.state.isLoading) {
        return (
          <>
            <ItemFakePost />
            <ItemFakePost />
            <ItemFakePost />
          </>
        );
      } else if (!this.state.isLoading) {
        return this.state.listPosts.map((post, i) => (
          <ItemPost onOpenModal={this.handleOpenModal} key={post.id.toString()+'-'+i.toString()} post={post} />
        ))
      } else {
        return '';
      }
    };

    const renderMessages = () => {
      let message;

      if(!!this.state.formError){
        message = this.state.formError;
      } else if (this.state.formState.isSubmittedSuccessful){
        message = 'Post ajouté';
      } else {
        return;
      }

      return (
        <div className={'mb-2 font-medium ' + (!!this.state.formError ? 'text-red-600' : 'text-green-600')}>
          {message}
        </div>
      );
    };

    return (
      <>
        {renderTitle()}

        <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 xl:grid-cols-4 gap-6">

          <div className="px-4 py-2 bg-white border border-gray-200 rounded-lg shadow">
            <h2 className="mb-4 text-xl">Nouvelle publication</h2>

            {renderMessages()}

            <form className="form-new-post" onSubmit={this.handleSubmit}>
              <label className="font-medium" htmlFor="title">Titre</label>
              <input disabled={this.state.isLoading} id="title" name="title" className="new-post-title" type="text" value={this.state.formValue.title} onChange={this.handleChange} autoComplete="off" />
              <br/>
              <label className="font-medium" htmlFor="body">Contenu</label>
              <textarea disabled={this.state.isLoading} id="body" name="body" className="new-post-body" value={this.state.formValue.body} onChange={this.handleChange} autoComplete="off"></textarea>
              <br/>
              <button className="mb-2" disabled={this.state.formState.isSubmitting} type="submit">Ajouter</button>
            </form>
          </div>

          {renderPostsList()}

          <ModalComments onCloseModal={this.handleCloseModal} openModal={this.state.openModalComments} postId={this.state.modalCommentsPostId} />
        </div>
      </>
    )
  }
}
