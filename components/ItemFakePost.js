import React from 'react'

export default class ItemFakePost extends React.Component{
  render(){
    return(
      <>
        <div className="flex flex-col h-full justify-between p-4 bg-white border border-gray-200 rounded-lg shadow animate-pulse" aria-label="Chargement des données en cours...">
          <div className="post-content">
            <span className="block h-6 w-3/4 mb-6 bg-gray-400 rounded-full" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-full mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-full mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
            <span className="block h-2 w-1/3 mb-4 bg-gray-300 rounded-lg" aria-hidden="true">&nbsp;</span>
          </div>
          <span className="block place-self-center h-2 w-2/3 bg-indigo-300 rounded-lg" aria-hidden="true">&nbsp;</span>
        </div>
      </>
    )
  }
}
