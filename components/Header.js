import Link from 'next/link'

export default function Header() {
    return (
      <header className="w-full p-6 bg-gray-800 text-white">
        <div className="container mx-auto flex">
          <div className="text-3xl md:text-4xl">
            <Link href="/">
                <a className="hover:underline">App Test ReactJS</a>
            </Link>
          </div>
          <nav className="flex-grow text-right">
            <Link href="/">
              <a className="nav-item">Liste d'utilisateurs</a>
            </Link>
          </nav>
        </div>
      </header>
    )
}
