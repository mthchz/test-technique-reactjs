import React from 'react'

export default class ItemPost extends React.Component{
  constructor(props) {
    super(props);

    this.handleOpenModal = this.handleOpenModal.bind(this);
  }

  handleOpenModal(){
      this.props.onOpenModal(this.props.post.id);
  }

  render(){
    return(
      <>
        <div className="flex flex-col h-full justify-between p-4 bg-white border border-gray-200 rounded-lg shadow">
          <div className="post-content">
            <h3 className="text-2xl mb-2">{this.props.post.title}</h3>
            <p className="mb-4">{this.props.post.body}</p>
          </div>
          <button onClick={this.handleOpenModal} className="text-sm text-center border-t border-gray-200">Voir les commentaires</button>
        </div>
      </>
    )
  }
}
